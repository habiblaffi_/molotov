import React, { useState, useEffect } from 'react';
import { FlatList, Platform, Text, View, StyleSheet, Image, ScrollView } from 'react-native';
import Cocktail from "./components/Cocktail.js"

export default function App() {
    const [cocktails, setCocktails] = useState(null);

    const Molotov = () => {
        fetch(`https://www.thecocktaildb.com/api/json/v1/1/random.php`)
            .then(function(response){
                return response.json();
            }).then(function(response){
            console.log(response);
            setCocktails(response.drinks)})
            console.log(cocktails);
    }

    useEffect( () => {
        Molotov();
    }, [])

  return (
    <View style={styles.container}>
        <FlatList
            onEndReached={Molotov}
            data={cocktails}
            renderItem={({item, index}) => {
                return (<Cocktail ImageURL={item.strDrinkThumb} nom={item.strDrink} key={index} />)
            }}
            keyExtractor={(item) => item.id}
        />
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
    },
    paragraph: {
        fontSize: 18,
        textAlign: 'center',
    },
});
